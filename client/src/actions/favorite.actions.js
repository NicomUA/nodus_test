import ActionTypes from './action.types';

export const addFavorite = (item) => ({
  type: ActionTypes.ADD_FAVORITE,
  item
});

export const removeFavorite = (item) => ({
  type: ActionTypes.REMOVE_FAVORITE,
  item
});

export const clearFavorite = () => ({
  type: ActionTypes.CLEAR_FAVORITE
});

export const loadFavorites = () => ({
  type: ActionTypes.LOAD_FAVORITE
})

export const saveFavorites = () => ({
  type: ActionTypes.SAVE_FAVORITE
})