import ActionTypes from './action.types';
import axios from 'axios';

export const setArticles = (data) => {
  return {
    type: ActionTypes.SET_ARTICLES,
    payload: {
      idlist: data.idlist,
      articles: data.articles
    }
  }
}

export const getArticles = (searchText) => dispatch => {
  dispatch(articlesLoading(true));
  axios
    .get('http://localhost:3000/api/search/' + searchText)
    .then((resp) => {
      let articlesData = {
        idlist: [],
        articles: []
      };

      if (resp.data.idlist.length != 0) {
        let result = resp.data.idlist.map(id => resp.data.articles[id]);
        articlesData = { idlist: resp.data.idlist, articles: result };
      }
      
      dispatch(setArticles(articlesData));
      dispatch(articlesLoading(false));
    });
};

export const setArticle = (text) => ({
  type: ActionTypes.ARTICLE,
  payload: {
    text
  }
})

export const getArticle = (id) => dispatch => {
  axios
    .get('http://localhost:3000/api/article/' + id)
    .then((resp) => {
      dispatch(setArticle(resp.data.text))
    });
}

export const articlesLoading = (isLoading) => ({
  type: ActionTypes.ARTICLES_LOADING,
  isLoading
})
