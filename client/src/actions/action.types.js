const ActionTypes = {
  // search actions
  SEARCH_TEXT: '[search] set search text',
  SEARCH_BY_TITLE: '[search] set search by title',
  SEARCH_BY_AUTHOR: '[search] set search by author',
  
  // article actions
  ARTICLES_LOADING: '[articles] set loading in progress',
  GET_ARTICLES: '[articles] get articles from API',
  SET_ARTICLES: '[articles] set articles',
  ARTICLE: '[articles] set article to read',

  //Favorite actions
  ADD_FAVORITE:'[favorite] add item',
  REMOVE_FAVORITE:'[favorite] remove item',
  CLEAR_FAVORITE: '[favorite] clear all favorite',
  SAVE_FAVORITE: '[favorite] save to storage',
  LOAD_FAVORITE: '[favorite] get from storage',
}

export default ActionTypes;