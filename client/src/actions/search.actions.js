import ActionTypes from './action.types';


export const setSearchText = text => ({
  type: ActionTypes.SEARCH_TEXT,
  text
});

export const setSearchByTitle = () => ({
  type: ActionTypes.SEARCH_BY_TITLE
});

export const setSearchByAuthor = () => ({
  type: ActionTypes.SEARCH_BY_AUTHOR
});

