import React from 'react';
import ReactDOM from 'react-dom';

import { library } from "@fortawesome/fontawesome-svg-core";
import { faSearch, faTrashAlt, faFileAlt } from "@fortawesome/free-solid-svg-icons";

library.add(faSearch, faTrashAlt, faFileAlt);

import './styles/style.scss';

import NodusReader from './components/NodusReader';
import { Provider } from "react-redux";
import store from "./store";

const App = ()=> (
  <Provider store={store}>
    <NodusReader />
  </Provider>
)

ReactDOM.render(<App />, document.getElementById('app'));






