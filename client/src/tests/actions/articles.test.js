import ActionTypes from '../../actions/action.types';
import {
  setArticle,
  setArticles,
  articlesLoading
} from '../../actions/articles.actions';

test("It should set loading in progress", () => {
  let result = articlesLoading(true);

  expect(result).toEqual({
    type: ActionTypes.ARTICLES_LOADING,
    isLoading: true
  });
});

test("It should set loading not in progress", () => {
  let result = articlesLoading(false);

  expect(result).toEqual({
    type: ActionTypes.ARTICLES_LOADING,
    isLoading: false
  });
});

test("It should set article", () => {
  let result = setArticle('article text');

  expect(result).toEqual({
    type: ActionTypes.ARTICLE,
    payload: {
      text: 'article text'
    }
  });
});

test("It should set article", () => {
  const data = {
    idlist: ['1','2'],
    articles: [{id:1},{id:2}]
  }
  let result = setArticles(data);

  expect(result).toEqual({
    type: ActionTypes.SET_ARTICLES,
    payload: data
  });
});

