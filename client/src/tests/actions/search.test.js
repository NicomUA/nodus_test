import ActionsTypes from '../../actions/action.types';
import { 
  setSearchText, 
  setSearchByTitle, 
  setSearchByAuthor } from '../../actions/search.actions';

test('It should set search text', ()=>{
  const result = setSearchText('test');
  expect(result).toEqual({
    type: ActionsTypes.SEARCH_TEXT,
    text: 'test'
  });
});

test('It should set search by title', ()=>{
  const result = setSearchByTitle();
  expect(result).toEqual({
    type: ActionsTypes.SEARCH_BY_TITLE
  });
});

test('It should set search by author', ()=>{
  const result = setSearchByAuthor();
  expect(result).toEqual({
    type: ActionsTypes.SEARCH_BY_AUTHOR
  });
});