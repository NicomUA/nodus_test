import React, { Fragment} from 'react';
import { connect } from 'react-redux';

let Reader = (props) => {
  let text = "No data to show";
  if (props.article) {
    text = props.article.split('\n').map((line, key)=>{
      return <Fragment key={key}>{line}<br /></Fragment>
    });
  }

  return (
    <div className="reader">
      {text}
    </div>
  );
}

const mapStateToProps = state => ({
  article: state.articles.article
})

export default connect(mapStateToProps)(Reader); 