import React from 'react';
import { connect } from 'react-redux';
import { ListGroup } from "reactstrap";

import ArticleItem from './ArticleItem';


let ArticlesList = (props) => {
  let Items = 'Start search from results';

  if(props.list.length > 0) {
    Items = props.list.map((item, key) => (
      <ArticleItem key={item.uid} article={item} />
    ));
  }

  return (
    <ListGroup>
      {props.isLoading && <div>Loading...</div>}
      {!props.isLoading && Items}
    </ListGroup>
  );
}

const mapStateToProps = state =>({
  list: state.articles.items,
  isLoading: state.articles.isLoading
})

export default connect(mapStateToProps)(ArticlesList);