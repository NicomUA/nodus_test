import React from 'react';
import { connect } from 'react-redux';
import { getArticle } from '../actions/articles.actions';
import {
  ListGroupItem,
  ListGroupItemHeading,
  ListGroupItemText,
  Badge
} from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

class ArticleItem extends React.Component{
  render(){
    let renderAuthor = (authors) => {
      if(authors.length === 0) return '';

      return authors.map((author) => (<span>{author.name} </span>));
    }
    
    return (
    <ListGroupItem>
      <ListGroupItemHeading>
          {this.props.article.title}
      </ListGroupItemHeading>
      <ListGroupItemText>
          Authors: {this.props.article.authors && renderAuthor(this.props.article.authors)}
      </ListGroupItemText>
      <Badge pill color="info"
        className="articleRead" 
        onClick={(e) => this.props.getArticle(this.props.article.uid)}>
        Read article
      </Badge>

    </ListGroupItem>
    )
  } 
}

const mapStateToProps = () => ({
  getArticle: getArticle
});


export default connect(mapStateToProps, { getArticle })(ArticleItem);