import React from 'react';
import { Container, Row, Col } from 'reactstrap';

import Header from './Header';
import Reader from './Reader';
import ArticlesList from "./ArticlesList";
import FavoriteList from './FavoriteList';

export default class NodusReader extends React.Component{
  render() {
    return <div>
        <Header />
        <Container>
          <Row>
            <Col xs="3">
              <FavoriteList />
            </Col>
            <Col xs="4">
              <ArticlesList />
            </Col>
            <Col xs="5">
              <Reader />
            </Col>
          </Row>
        </Container>
      </div>;
  }
}