import React from 'react';
import { connect } from 'react-redux';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { ListGroupItem, Badge } from "reactstrap";

import { getArticles } from '../actions/articles.actions';
import { removeFavorite } from "../actions/favorite.actions";


class FavoriteItem extends React.Component {
  render() {
    return (
      <ListGroupItem>
        <a onClick={this.handleClick}>{this.props.item}</a>
        <Badge pill color="primary"
          className="articleRead mr-1 ml-1"
          onClick={e => this.props.searchFor(this.props.item)}>
          <FontAwesomeIcon icon="search" />
        </Badge>
        <Badge pill color="danger"
          className="articleRead"
          onClick={e => this.props.removeFromFavorite(this.props.item)}>
          <FontAwesomeIcon icon="trash-alt" />
        </Badge>
        </ListGroupItem>
      ); 
  }; 
}

const mapDispatchToProps = dispatch => ({
  searchFor: item => dispatch(getArticles(item)),
  removeFromFavorite: item => dispatch(removeFavorite(item)),
});

export default connect( null, mapDispatchToProps )(FavoriteItem);