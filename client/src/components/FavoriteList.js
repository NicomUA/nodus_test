import React from 'react';
import { connect } from 'react-redux';
import FavoriteItem from './FavoriteItem';
import { ListGroup } from 'reactstrap'
import { loadFavorites } from '../actions/favorite.actions';

class FavoriteList extends React.Component {
  componentWillMount() {
    this.props.loadFavorites();
  }
  
  render() {
    let Items = 'No Favorites';
    if(this.props.list.length > 0) {
      Items = this.props.list.map((item, key) => (
        <FavoriteItem key={key} item={item}></FavoriteItem>
      ))
    }
    return (
      <div>
        <h2>Favorite list:</h2>
        <ListGroup>{Items}</ListGroup>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  list: state.favorite.items,
});

const mapDispatchToProps = dispatch => ({
  loadFavorites: () => dispatch(loadFavorites())
});


export default connect(mapStateToProps, mapDispatchToProps)(FavoriteList);