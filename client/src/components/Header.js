import React from 'react';
import {
  Navbar,
  NavbarBrand,
} from 'reactstrap';
import SearchBar from './SearchBar';

const Header = (props) => {
  return <div className="mb-3">
      <Navbar color="dark" dark>
        <NavbarBrand href="/">Nodus PubMed Reader</NavbarBrand>
        <SearchBar searchArticles={props.searchArticles} />
      </Navbar>
    </div>;
};

export default Header;