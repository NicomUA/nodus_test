import React, {Fragment} from "react";
import { connect } from 'react-redux';
import { getArticles } from '../actions/articles.actions';
import PropTypes from 'prop-types';

import { Button, Input, Form, FormGroup } from "reactstrap";
import { addFavorite } from '../actions/favorite.actions';

class SearchBar extends React.Component {
  state = {
    searchBy: 'title',
  }
  onSubmit = (e)=>{
    e.preventDefault();
    const searchText = e.target.elements.search.value.trim();
    
    if(searchText !== '') {
      this.props.getArticles(searchText);
    }
  }
  addToFav = (e) => {
    this.props.addFavorite(this.state.searchText);
  }

  handleChange = (e)=> {
    const text = e.target.value.trim();

    if(text !== '') {
      this.setState(()=>({
        searchText: text
      }))
    }
  }

  handleSearchChange = (e) => {
    const value = e.target.value;
    console.log(value);
  }

  render() {
   return  (
     <Fragment>
       <Form onSubmit={this.onSubmit} inline>
         <FormGroup className="mb-0">
          <Input 
            onChange={this.handleChange}
            id="search"
            type="text" 
            name="search"
            placeholder="Start typing article title"
            className="mr-md-2"
            ></Input> 
        </FormGroup>
        <Button type="submit" className="mr-1">Search</Button>
        <Button type="button" onClick={this.addToFav}>Add To Favorite</Button>
      </Form>
     </Fragment>
  )}
}

SearchBar.propTypes = {
  getArticles: PropTypes.func.isRequired,
  addFavorite: PropTypes.func.isRequired,
  searchText: PropTypes.string
};

const mapStateToProps = state  => ({
  searchText: state.searchText
});

const mapDispatchToProps = dispatch => ({
  getArticles: text => dispatch(getArticles(text)),
  addFavorite: text => dispatch(addFavorite(text))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchBar);



