import actionTypes from '../actions/action.types';

const INIT_STORE = {
  items: [],
  idlist: [],
  article: null,
  isLoading: false
}

const articleReducer = (state = INIT_STORE, action) => {
  switch (action.type) {
    case actionTypes.SET_ARTICLES:
      return {
        ...state,
        items: action.payload.articles,
        idlist: action.payload.idlist
      }
    case actionTypes.ARTICLE:
      return {
        ...state,
        article: action.payload.text
      }
    case actionTypes.ARTICLES_LOADING:
      return {
        ...state,
        isLoading: action.isLoading
      }
    default:
      return state;
  }
}

export default articleReducer;