import actionTypes from '../actions/action.types';

const INIT_STORE = {
  items: [],
}

const _STORAGE_KEY = "favorites";

const saveToLocalStorage = (items)=>{
  const itemsToSave = JSON.stringify(items);
  localStorage.setItem("favorites", itemsToSave);
}

const favoriteReducer = (state = INIT_STORE, action) => {
  switch (action.type) {
    case actionTypes.ADD_FAVORITE:
      if(state.items.indexOf(action.item) !== -1){
        return state;
      }

      let itemsWithNewItem = state.items.concat([action.item]);
      saveToLocalStorage(itemsWithNewItem);
      
      return {
        ...state,
        items: itemsWithNewItem
      }
      case actionTypes.REMOVE_FAVORITE:
      let itemsWithoutRemoved = state.items.filter((item) => item !== action.item);
      saveToLocalStorage(itemsWithoutRemoved);

      return {
        ...state,
        items: itemsWithoutRemoved
      }
    case actionTypes.CLEAR_FAVORITE:
      saveToLocalStorage([]);
      return {
        ...state,
        items: []
      }
    case actionTypes.LOAD_FAVORITE:
      let loadedItems = localStorage.getItem('favorites');
      let items = JSON.parse(loadedItems);
      
      if (items && items.length > 0) {
        return { ...state.length, items };
      }

    default:
      return state;
  }
}

export default favoriteReducer;