import { combineReducers } from 'redux'
import searchReducer from './search.reducer'
import articleReducer from './article.reducer';
import favoriteReducer from './favorite.reducer';

export default combineReducers({
  search: searchReducer,
  articles: articleReducer,
  favorite: favoriteReducer
});