import actionTypes from '../actions/action.types';

const searchReducer = (state = [], action) => {
  switch (action.type){
    case actionTypes.SEARCH_TEXT: 
      return {
        ...state,
        searchText: action.text 
      }
    case actionTypes.SEARCH_BY_TITLE:
      return {
        ...state,
        searchBy: 'title'
      }
    case actionTypes.SEARCH_BY_AUTHOR:
      return {
        ...state,
        searchBy: 'author'
      }
    default:
      return state;
  }
}

export default searchReducer;