# Deploy
  npm install
  npm run serve-api
  
# Development
  cd ./client && npm install && cd ..
  npm run dev

# Build UI
  cd ./client
  npm run build
