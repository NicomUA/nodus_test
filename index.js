const express = require('express');
const app = express();
const path = require('path');
const serverConst = require('./server/server.const');

// Set port from env or default 
const _PORT = process.env.PORT || serverConst.PORT;

// init static server
app.use('/', express.static(path.join(__dirname,'./public/')))

// inject routes
require('./server/routes')(app);

// start server
app.listen(_PORT,()=>{
  console.log(`server started at port: ${_PORT}`);
})