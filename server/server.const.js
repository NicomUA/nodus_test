// Server constants 

module.exports = (()=>{ 
  return { 
    PORT: 3000,
    searchUrl: "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed&retmode=json&term=", 
    summeryUrl: "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=pubmed&retmode=json&rettype=abstract&id=", 
    fetchUrl: "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&retmode=text&rettype=abstract&id=", 
    messages: {
      error: { code: 503, message: "Internal server error" }, 
      wrongSearch: {code: 200, message: 'Wrong search parameters'}
    },
  }
})();