const router = require("express").Router();
const axios = require("axios");

const serverConst = require("../server.const");
const fetchUrl = serverConst.fetchUrl;

/** 
 * Get Article from PubMed API
 * @param {number} articleId
*/

function getArticle(articleId) {
  try {
    return axios.get(`${fetchUrl}${articleId}`)
      .then(respData => {
        return respData.data;
      })
      .catch(err => {
        return Promise.reject({
          status: err.response.status,
          message: err.response.statusText
        })
      });
  }
  catch (e) {
    return Promise.reject();
  }
}

/** 
 * Get Article route
 * @param {number} articleId
*/

router.get("/:articleId", (req, resp) => {
  if (!req.params.articleId) {
    resp.send(serverConst.message.wrongSearch);
  }

  getArticle(req.params.articleId)
    .then(result => {
      let data = {
        id: req.params.articleId,
        text: result
      }
      resp.json(data);
    })
    .catch(err => {
      if(err) {
        resp.status(400).json(serverConst.messages.wrongSearch);
      }
      else {
        resp.status(503).json(serverConst.messages.error)
      } 
    });
});

module.exports = router;