const router = require("express").Router();
const axios = require("axios");

const serverConst = require("../server.const");
const searchURL = serverConst.searchUrl;
const summeryUrl = serverConst.summeryUrl;

/**
  *  Get Articles from PubMed API
  *  @param {number} articleId
  */

function getArticles({ idlist, count } = {}) {
  if (!idlist || idlist.length === 0) {
    return Promise.reject();
  }

  try {
    return axios.get(`${summeryUrl}${idlist.join(",")}`).then(respData => {
      return {
        idlist,
        articles: respData.data.result
      };
    });
  } catch (e) {
    console.log(e);
    return Promise.reject(e);
  }
}

/**
  *  Search aricles in PubMed API by title
  *  @param {string} term
  */

function doSearch(term) {
  return axios.get(`${searchURL}${encodeURIComponent(term)}`).then(respData => {
    const searchResult = respData.data.esearchresult;
    let { idlist, count, querytranslation } = searchResult;
    return { idlist, count, querytranslation };
  });
}

/**
  *  Search route
  *  @param {string} term
  */

router.get("/:term", (req, resp) => {
  if (!req.params.term) {
    resp.send(serverConst.message.wrongSearch);
  }

  doSearch(req.params.term)
    .then(getArticles)
    .then(result => {
      resp.json(result);
    })
    .catch(err => resp.status(503).json(serverConst.messages.error));
});

/**
  *  Search aricles in PubMed API by author
  *  @param {string} author
  */

function doSearchByAuthor(author) {
  try {
    return axios
      .get(`${searchURL}${encodeURIComponent(author)}[author]`)
      .then(respData => {
        const searchResult = respData.data.esearchresult;
        let { idlist, count, querytranslation } = searchResult;
        return { idlist, count, querytranslation };
      })
      .catch(err => console.log(err));
  } catch (e) {
    console.log(e);
    return Promise.reject();
  }
}

/**
  *  Search by author route
  *  @param {string} author
  */

router.get("/author/:author", (req, resp) => {
  if (!req.params.author) {
    resp.send(serverConst.message.wrongSearch);
  }

  doSearchByAuthor(req.params.author)
    .then(getArticles)
    .then(result => {
      resp.json(result);
    })
    .catch(err => resp.status(503).json(serverConst.messages.error));
});

module.exports = router;
