const searchAPI = require('./search');
const articleAPI = require('./article');
const storeAPI = require('./store');

module.exports = (app)=>{
  
  // bind search API
  app.use('/api/search', searchAPI);

  //bind article API
  app.use('/api/article', articleAPI)

  //bind store logic
  app.use('/api/user', storeAPI);
}



