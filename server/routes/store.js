const router = require('express').Router();
const uuid = require('uuid');

router.get('/', (req, resp)=>{
  resp.json({status:'ok',id:  uuid()})
})

router.post('/', (req, resp)=>{
  resp.send('ok');
});


module.exports = router;